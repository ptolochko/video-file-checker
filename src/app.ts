#!/usr/bin/env node

import * as fs from "fs-jetpack"
import * as _ from "lodash"
import * as flat from "flat"
import {standardVideoInfo} from "./standardVideoInfo"

const MP4Box = require('../lib/mp4box.all.js')

main()

function showErrorAndExit(...args:any[]){
    console.error("Error", ...args)
    process.exit(1)
}

function readVideoInfo(path: string){
    return fs.readAsync(path, "buffer").then(file => {
        return new Promise((resolve, reject) => {
            const mp4boxfile = MP4Box.createFile()
            mp4boxfile.onError = (e:any) => reject(e)
            mp4boxfile.onReady = (info:any) => resolve(info)
            const arrayBuffer = new Uint8Array(file!).buffer as any
            arrayBuffer.fileStart = 0
            mp4boxfile.appendBuffer(arrayBuffer)
            mp4boxfile.flush()
        })
    })
}

function difference(object: any, base: any) {
   function changes(object: any, base: any) {
        let arrayIndexCounter = 0;
        return _.transform(object, (result, value, key) => {
            const baseValue = base[key]
            if (!_.isEqual(value, baseValue)) {
                let resultKey = _.isArray(base) ? arrayIndexCounter++ : key;
                result[resultKey] = (_.isObject(value) && _.isObject(baseValue)) ? changes(value, baseValue) : baseValue;
            }
        });
    }
    return changes(object, base);
}

function checkVideoInfo(path: any, info: any) {
    const diff = difference(flat.flatten(standardVideoInfo), flat.flatten(info))
    if(!_.isEmpty(diff)){
        showErrorAndExit(path, diff)
    }
}

async function main(){
    const videoInfoList = await Promise.all(fs.find({files: true, matching: "*intro*.mp4"})
        .map(x => readVideoInfo(x).then(vi => [x,vi])))
    videoInfoList.forEach(([path, info]) => {
        checkVideoInfo(path, info)
    })
}






