export const standardVideoInfo = {
    "hasMoov": true,
    "timescale": 90000,
    "isFragmented": false,
    "isProgressive": true,
    "hasIOD": false,
    "brands": ["mp42", "mp42", "mp41"],
    "tracks": [{
        "id": 1,
        "name": "\u001fMainconcept Video Media Handler",
        "references": [],
        "edits": [{"media_time": 1001, "media_rate_integer": 1, "media_rate_fraction": 0}],
        "movie_timescale": 90000,
        "layer": 0,
        "alternate_group": 0,
        "volume": 0,
        "matrix": new Int32Array([ 65536, 0, 0, 0, 65536, 0, 0, 0, 1073741824 ]),
        "track_width": 1920,
        "track_height": 1080,
        "timescale": 24000,
        "codec": "avc1.4d402a",
        "kind": {"schemeURI": "", "value": ""},
        "language": "eng",
        "type": "video",
        "video": {"width": 1920, "height": 1080}
    }],
    "mime": "video/mp4; codecs=\"avc1.4d402a\"; profiles=\"mp42,mp41\""
}
